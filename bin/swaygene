#!/bin/bash

if [[ -f ~/.generc ]] ; then
    . ~/.generc
fi

remove_leading_zeros() {
    local s="$1"
    local number=0

    # force decimal (base 10), side-effect: leading zeros will be removed;
    # does not evaluate wether this is a number
    number=$(( 10#$s ))

    echo "$number"
}

get_load() {
    content=""

    # get virtual cpu count and loadavg from proc-filesystem
    proc_cpuinfo_processor=$( cat /proc/cpuinfo | grep processor | tail -n 1 )
    if [[ -z "$proc_cpuinfo_processor" ]] ; then
        proc_cpuinfo_processor=0
    fi
    proc_loadavg=$( cat /proc/loadavg )

    local i=0
    for loadavg in $proc_loadavg ; do
        i=$(( i+1 ))
        loadavg=${loadavg/./}
        if [[ "$i" -eq 1 ]] ; then
            load1="$loadavg"
        fi
    done
    load_max=${proc_cpuinfo_processor#*: }
    load_max=$(( load_max+1 ))
    load_max=$(( load_max*2 ))
    load1=$( remove_leading_zeros "$load1" )

    load1_f=$(( load1/100 ))

    if [[ "${load1_f}" -gt 4 ]] ; then
        load="⬣${load1_f}"
    else
        load="⎔ ${load1_f}"
    fi
    echo "${load}"
}

get_mem() {
    # get ram and swap usage from proc-filesystem
    proc_meminfo=$( cat /proc/meminfo )

    # defaults (1 kB -> 100%)
    mt=1
    mf=1
    st=1
    sf=1

    # init
    mt_is_next=0
    mf_is_next=0
    st_is_next=0
    sf_is_next=0
    local i=0
    for m in ${proc_meminfo} ; do
        #echo "[DEBUG] m = $m"
        i=$(( i+1 ))
        if [[ "${mt_is_next}" -eq 1 ]] ; then
            mt="$m"
            mt_is_next=0
        elif [[ "${mf_is_next}" -eq 1 ]] ; then
            mf="$m"
            mf_is_next=0
        elif [[ "${st_is_next}" -eq 1 ]] ; then
            st="$m"
            st_is_next=0
        elif [[ "${sf_is_next}" -eq 1 ]] ; then
            sf="$m"
            sf_is_next=0
        fi
        mtm=".*MemTotal.*"
        mfm=".*MemFree.*"
        stm=".*SwapTotal.*"
        sfm=".*SwapFree.*"
        if [[ "$m" =~ $mtm ]] ; then
            #echo "[DEBUG] matched: $m"
            mt_is_next=1
        elif [[ "$m" =~ $mfm ]] ; then
            #echo "[DEBUG] matched: $m"
            mf_is_next=1
        elif [[ "$m" =~ $stm ]] ; then
            #echo "[DEBUG] matched: $m"
            st_is_next=1
        elif [[ "$m" =~ $sfm ]] ; then
            #echo "[DEBUG] matched: $m"
            sf_is_next=1
        fi
    done

    #echo "[DEBUG] mt = ${mt}"
    #echo "[DEBUG] mf = ${mf}"
    #echo "[DEBUG] st = ${st}"
    #echo "[DEBUG] sf = ${sf}"

    mp_f=$(( (mt-mf)*100/mt ))
    sp_f=$(( (st-sf)*100/st ))
    #echo "[DEBUG]  ram = ${mp}"
    #echo "[DEBUG] swap = ${sp}"

    if [[ "${sp_f}" -gt 50 ]] ; then
        memory="⬟ ${mp_f} ${sp_f}"
    else
        memory="⬠ ${mp_f} ${sp_f}"
    fi
    echo "${memory}"
}

get_uptimes() {
    up_days=0
    idle_ratio=0

    # get virtual cpu count from proc-filesystem
    proc_cpuinfo_processor=$( cat /proc/cpuinfo | grep processor | tail -n 1 )
    if [[ -z "$proc_cpuinfo_processor" ]] ; then
        proc_cpuinfo_processor=0
    fi
    proc_cpuinfo_processor=${proc_cpuinfo_processor#*: }

    local r_proc_uptime="$( cat /proc/uptime )"
    local i=0
    for t_proc_uptime in $r_proc_uptime ; do
        t_proc_uptime="${t_proc_uptime%.*}"
        #echo "[DEBUG] ${t_proc_uptime}"
        if [[ "$i" -eq 0 ]] ; then
            up_days="$(( t_proc_uptime/60/60/24 ))"
        fi
        i=$(( i+1 ))
    done

    if [[ "${up_days}" -gt 90 ]] ; then
        uptime_days="⭓ ${up_days}"
    else
        uptime_days="⭔ ${up_days}"
    fi
    echo "${uptime_days}"
}

get_cputemp() {
    temps=$( cat /sys/class/thermal/thermal_zone?/temp )
    temp_types=$( cat /sys/class/thermal/thermal_zone?/type )

    local thermal_zone_index=1
    for t in ${temp_types} ; do
        #echo "[DEBUG] type[${thermal_zone_index}] = $t"
        if [[ "${t}" == "x86_pkg_temp" ]] ; then
            cpu_type_index=${thermal_zone_index}
        fi
        thermal_zone_index=$(( thermal_zone_index+1 ))
    done

    #echo "[DEBUG] cpu_type_index = ${cpu_type_index}"

    local temp_index=1
    for tt in ${temps} ; do
        #echo "[DEBUG] temp[${temp_index}] = $tt"
        if [[ "${temp_index}" -eq "${cpu_type_index}" ]] ; then
            temp=$tt
            break
        fi
        temp_index=$(( temp_index+1 ))
    done

    temp=$(( temp/1000 ))

    if [[ "${temp}" -gt 70 ]] ; then
        cputemp="⬢${temp}"
    else
        cputemp="⬡ ${temp}"
    fi
    echo "${cputemp}"
}

# main loop
sleep 1
minute=0
while true ; do
    # luft
    if [[ "${minute}" -ge 6 ]] ; then
        luft=$( . /home/poinck/gits/gene/bin/swaygene-luft )
        minute=0
    fi

    # load
    l=$( get_load )

    # memory
    m=$( get_mem )

    # uptime
    u=$( get_uptimes )

    # cputemp
    c=$( get_cputemp )

    # date
    d=$(date +"%a %F %H:%M")

    # output
    echo "${luft}   ${l}   ${m}   ${u}   ${c}   ${d} "

    sleep 10
    minute=$(( minute+1 ))
done
