const St = imports.gi.St;
const Main = imports.ui.main;
const ml = imports.mainloop;
const glib = imports.gi.GLib;
const ba = imports.byteArray;

let timeout;
let bins = []
let labels = []

const homedir = glib.get_home_dir();

function update_text() {
    /** read from elements.csv and parse:
     * #272822; background: orange; border-radius: 4px,SEC
     * #6F804E; background: #2F400E; border-radius: 5px; margin-left: 5px; border: 2px solid #3F501E,🦙
     */

    try {
        // add text of all 10 elements from file
        var [ok, u8_content] = glib.file_get_contents(
            homedir + "/.gene/elements.csv"
        )
        var content = ba.toString(u8_content)
        //global.log("ok (typeof):" + typeof ok)
        //global.log("content (typeof):" + typeof content)
        //global.log("content[0]:" + content[0])
        let lines = content.split("\n")
        let element_count = Math.min(10, lines.length - 1)
        for (let i = 0; i < element_count; i++) {
            // color,text
            // #a5a701,test
            let [color, text] = lines[i].split(",")
            labels[i].set_text(" " + text + " ");
            if (color != "") {
                labels[i].set_style("margin-top: 6px; margin-left: 5px; color: " + color + ";")
            }
            else {
                // default Adwaita color
                labels[i].set_style("margin-top: 6px; color: #cccccc;")
            }
        }
    }
    catch (e) {
        // EE = extension exception
        labels[9].set_text("EE");
        labels[9].set_style("margin-top: 6px; color: orange;")
        global.log(e)
    }

    return true;
}

function init() {
    // create 10 elements
    // - element signature: color:text in .gene/elements.csv
    for (let i = 0; i < 10; i++) {
        bin = new St.Bin({
            style_class: "panel-button"
        });
        label = new St.Label({
            style_class: "panel-button-text",
            text: ""
        });
        bin.set_child(label)
        bins.push(bin)
        labels.push(label)

        labels[0].set_text("start ..");
        labels[0].set_style("margin-top: 6px; color: #cccccc;")
    }
}

function enable() {
    // insert 10 predefined bins
    for (let i = 0; i < 10; i++) {
        Main.panel._rightBox.insert_child_at_index(bins[i], i + 1);
        // labels[i].set_text("")
    }

    // call update_text every 10 seconds
    timeout = ml.timeout_add_seconds(10.0, update_text);

    // TODO: start gene-collector async (like ocollector, to gather element
    // contents in ~/.gene/elements.csv)
}

function disable() {
    // remove 10 predefined bins
    for (let i = 0; i < 10; i++) {
        Main.panel._rightBox.remove_child(bins[i]);
    }

    ml.source_remove(timeout);
}
